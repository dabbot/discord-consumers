use audio::AudioClient;
use bot_lists::{
    model::Authorization as BotListsAuthorization,
    Client as BotListsClient,
};
use cache::Cache;
use crate::{
    env,
    prelude::*,
};
use dump::DumpClient;
use hyper::Client as HyperClient;
use hyper_tls::HttpsConnector;
use patron_api::PatronClient;
use queue::QueueClient;
use redis_async::client::{self as redis_client, PairedConnection};
use reqwest::r#async::Client;
use serenity::http::Client as SerenityHttpClient;
use std::sync::Arc;
use tokio::runtime::current_thread;

pub fn audio() -> AudioClient {
    AudioClient::new(
        Arc::new(Client::new()),
        env::lavalink_http_server_address(),
    )
}

pub fn bot_lists() -> BotListsClient {
    let auth = BotListsAuthorization {
        discord_bots_org: env::discord_bots_org_authorization(),
    };

    BotListsClient::new(
        auth,
        env::discord_user_id(),
        Arc::new(Client::new()),
        Arc::new(cache()),
    )
}

pub fn cache() -> Cache {
    Cache::new(Arc::new(redis()))
}

pub fn discord_api() -> SerenityHttpClient {
    let hyper = HyperClient::builder().build(HttpsConnector::new(4).unwrap());
    let mut serenity = SerenityHttpClient::new(
        Arc::new(hyper),
        Arc::new(env::discord_token()),
    ).expect("Error making serenity client");
    serenity.set_base_url(env::discord_proxy_address());
    serenity.skip_ratelimiter();

    serenity
}

pub fn dump() -> DumpClient {
    DumpClient::new(
        Arc::new(Client::new()),
        false,
        env::dump_post_address(),
        env::dump_authorization(),
    )
}

pub fn patron() -> PatronClient {
    PatronClient::new(
        Arc::new(Client::new()),
        false,
        env::patron_api_address(),
    )
}

pub fn queue() -> QueueClient {
    QueueClient::new(Arc::new(Client::new()), env::queue_address())
}

pub fn redis() -> PairedConnection {
    current_thread::block_on_all(redis_client::paired_connect(
        &env::redis_address(),
    )).expect("Error initializing connection to redis")
}
