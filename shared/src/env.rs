use std::{
    env,
    ffi::OsStr,
    net::{IpAddr, SocketAddr},
    str::FromStr,
};

pub struct Variables {
    pub discord_bots_org_authorization: Option<String>,
    pub discord_proxy_address: String,
    pub discord_token: String,
    pub discord_user_id: u64,
    pub dump_authorization: String,
    pub dump_display_address: String,
    pub dump_post_address: String,
    pub lavalink_http_server_address: String,
    pub patron_api_address: String,
    pub queue_address: String,
    pub redis_host: String,
    pub redis_port: u16,
}

impl Default for Variables {
    fn default() -> Self {
        Self {
            discord_bots_org_authorization: discord_bots_org_authorization(),
            discord_proxy_address: discord_proxy_address(),
            discord_token: discord_token(),
            discord_user_id: discord_user_id(),
            dump_authorization: dump_authorization(),
            dump_display_address: dump_display_address(),
            dump_post_address: dump_post_address(),
            lavalink_http_server_address: lavalink_http_server_address(),
            patron_api_address: patron_api_address(),
            queue_address: queue_address(),
            redis_host: redis_host(),
            redis_port: redis_port(),
        }
    }
}

pub fn discord_bots_org_authorization() -> Option<String> {
    var_maybe("DISCORD_BOTS_ORG_AUTHORIZATION")
}

pub fn discord_proxy_address() -> String {
    var("DISCORD_PROXY_ADDRESS")
}

pub fn discord_token() -> String {
    var("DISCORD_TOKEN")
}

pub fn discord_user_id() -> u64 {
    var("DISCORD_USER_ID").parse().expect("DISCORD_USER_ID not a u64")
}

pub fn dump_authorization() -> String {
    var("DUMP_AUTHORIZATION")
}

pub fn dump_display_address() -> String {
    var("DUMP_DISPLAY_ADDRESS")
}

pub fn dump_post_address() -> String {
    var("DUMP_POST_ADDRESS")
}

pub fn lavalink_http_server_address() -> String {
    var("LAVALINK_HTTP_SERVER_ADDRESS")
}

pub fn patron_api_address() -> String {
    var("PATRON_API_ADDRESS")
}

pub fn queue_address() -> String {
    var("QUEUE_ADDRESS")
}

pub fn redis_host() -> String {
    var("REDIS_HOST")
}

pub fn redis_port() -> u16 {
    var("REDIS_PORT").parse().expect("Invalid redis port")
}

pub fn redis_address() -> SocketAddr {
    let host = IpAddr::from_str(&redis_host()).expect("Invalid redis address");

    SocketAddr::new(host, redis_port())
}

fn var(name: impl AsRef<OsStr>) -> String {
    let name = name.as_ref();

    match env::var(name) {
        Ok(name) => name,
        Err(_) => {
            panic!("Missing environment variable: {}", name.to_string_lossy());
        },
    }
}

fn var_maybe(name: impl AsRef<OsStr>) -> Option<String> {
    env::var(name).ok()
}
