use audio::AudioClient as AudioClientInner;
use bot_lists::Client as BotListsClientInner;
use cache::Cache as CacheInner;
use core::ops::{Deref, DerefMut};
use crate::client;
use discord_fm::DiscordFm;
use dump::DumpClient as DumpClientInner;
use futures::compat::Future01CompatExt;
use patron_api::PatronClient as PatronClientInner;
use queue::QueueClient as QueueClientInner;
use radios::RadioList;
use serenity::{
    http::Client as SerenityHttpClient,
    model::channel::Message,
};
use std::error::Error;

pub struct AudioClient(AudioClientInner);

impl Default for AudioClient {
    fn default() -> Self {
        Self(client::audio())
    }
}

impl Deref for AudioClient {
    type Target = AudioClientInner;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for AudioClient {
    fn deref_mut(&mut self) -> &mut AudioClientInner {
        &mut self.0
    }
}

pub struct BotListsClient(BotListsClientInner);

impl Default for BotListsClient {
    fn default() -> Self {
        Self(client::bot_lists())
    }
}

impl Deref for BotListsClient {
    type Target = BotListsClientInner;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for BotListsClient {
    fn deref_mut(&mut self) -> &mut BotListsClientInner {
        &mut self.0
    }
}

pub struct CacheClient(CacheInner);

impl Default for CacheClient {
    fn default() -> Self {
        Self(client::cache())
    }
}

impl Deref for CacheClient {
    type Target = CacheInner;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for CacheClient {
    fn deref_mut(&mut self) -> &mut CacheInner {
        &mut self.0
    }
}

pub struct DiscordApiClient(SerenityHttpClient);

impl DiscordApiClient {
    pub async fn msg<'a>(
        &'a self,
        channel_id: u64,
        msg: impl Into<String> + 'a,
    ) -> Result<Message, Box<dyn Error>> {
        await!(self.send_message(channel_id, |mut m| {
            m.content(msg.into());

            m
        }).compat()).map_err(From::from)
    }

    pub async fn send<'a>(
        &'a self,
        channel_id: u64,
        msg: impl Into<String> + 'a,
    ) -> Result<(), Box<dyn Error>> {
        await!(self.msg(channel_id, msg))?;

        Ok(())
    }
}

impl Default for DiscordApiClient {
    fn default() -> Self {
        Self(client::discord_api())
    }
}

impl Deref for DiscordApiClient {
    type Target = SerenityHttpClient;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for DiscordApiClient {
    fn deref_mut(&mut self) -> &mut SerenityHttpClient {
        &mut self.0
    }
}

pub struct DiscordFmClient(DiscordFm);

impl Default for DiscordFmClient {
    fn default() -> Self {
        Self(DiscordFm::new().expect(
            "Failed to build discord.fm list",
        ))
    }
}

impl Deref for DiscordFmClient {
    type Target = DiscordFm;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for DiscordFmClient {
    fn deref_mut(&mut self) -> &mut DiscordFm {
        &mut self.0
    }
}

pub struct DumpClient(DumpClientInner);

impl Default for DumpClient {
    fn default() -> Self {
        Self(client::dump())
    }
}

impl Deref for DumpClient {
    type Target = DumpClientInner;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for DumpClient {
    fn deref_mut(&mut self) -> &mut DumpClientInner {
        &mut self.0
    }
}

pub struct PatronApiClient(PatronClientInner);

impl Default for PatronApiClient {
    fn default() -> Self {
        Self(client::patron())
    }
}

impl Deref for PatronApiClient {
    type Target = PatronClientInner;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for PatronApiClient {
    fn deref_mut(&mut self) -> &mut PatronClientInner {
        &mut self.0
    }
}

pub struct QueueClient(QueueClientInner);

impl Default for QueueClient {
    fn default() -> Self {
        Self(client::queue())
    }
}

impl Deref for QueueClient {
    type Target = QueueClientInner;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for QueueClient {
    fn deref_mut(&mut self) -> &mut QueueClientInner {
        &mut self.0
    }
}


pub struct RadioListClient(RadioList);

impl Default for RadioListClient {
    fn default() -> Self {
        Self(RadioList::new().expect("Err making radio list"))
    }
}

impl Deref for RadioListClient {
    type Target = RadioList;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for RadioListClient {
    fn deref_mut(&mut self) -> &mut RadioList {
        &mut self.0
    }
}
