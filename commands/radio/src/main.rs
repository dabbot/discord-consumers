#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;
use join_shared::JoinRequest;

#[derive(Default)]
pub struct Radio {
    audio: AudioClient,
    cache: CacheClient,
    discord: DiscordApiClient,
    queue: QueueClient,
    radios: RadioListClient,
    variables: Variables,
}

impl Radio {
    pub const NAME: &'static str = "radio";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        if cmd.args.is_empty() {
            let msg = format!(
                "View the radios here: <https://dabbot.org/radios>
To play a radio, use \
                 `{prefix}radio <name here>`.
For example, use `{prefix}radio Radio Here`",
                prefix=cmd.prefix,
            );

            return await!(self.discord.send(cmd.channel_id(), msg));
        }

        let query = cmd.args.join(" ");

        let radio = match self.radios.get(&query) {
            Some(radio) => radio,
            None => {
                let msg = format!(
                    "Invalid station! For usage & stations, use `{}radio`",
                    cmd.prefix,
                );

                return await!(self.discord.send(cmd.channel_id(), msg));
            },
        };

        let results = match await!(self.audio.search(radio.url.clone(), false)) {
            Ok(tracks) => tracks,
            Err(why) => {
                warn!("Err searching tracks for query '{}': {:?}", query, why);

                return await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error searching for that.",
                ));
            },
        };

        let radio = match results.tracks.first() {
            Some(radio) => radio,
            None => {
                return await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error loading that radio.",
                ));
            },
        };

        let req = JoinRequest {
            audio: &self.audio,
            bot_id: self.variables.discord_user_id,
            cache: &self.cache,
            channel_id: cmd.channel_id(),
            guild_id: cmd.guild_id,
            pop: false,
            queue: &self.queue,
            user_id: cmd.user_id(),
        };
        await!(join_shared::join(&req))?;

        let response = await!(self.audio.play(
            cmd.guild_id,
            radio.track.clone(),
        ))?;

        await!(self.discord.send(cmd.channel_id(), response.to_string()))
    }
}

command!(Radio);

fn main() {
    Radio::default().run();
}
