#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;
use join_shared::JoinRequest;
use std::fmt::Write;

#[derive(Default)]
pub struct Load {
    audio: AudioClient,
    cache: CacheClient,
    discord: DiscordApiClient,
    dump: DumpClient,
    queue: QueueClient,
    variables: Variables,
}

impl Load {
    pub const NAME: &'static str = "load";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let guild_id = cmd.guild_id;

        let query = match cmd.args.first() {
            Some(query) => query,
            None => {
                return await!(self.discord.send(
                    cmd.channel_id(),
                    "You need to say the URL to the queue dump!",
                ));
            },
        };

        let (host, uuid) = {
            let uuid = if query.starts_with("http") {
                let slash = match query.rfind('/') {
                    Some(slash) => slash,
                    None => {
                        return await!(self.discord.send(
                            cmd.channel_id(),
                            "That doesn't look like a valid load URL.",
                        ));
                    },
                };

                let slice = &query[slash + 1..];

                if slice.is_empty() {
                    return await!(self.discord.send(
                        cmd.channel_id(),
                        "That doesn't look like a valid load URL.",
                    ));
                }

                slice
            } else {
                &query
            };

            // Temporary paste.dabbot.org support.
            let host = if query.starts_with("https://paste.dabbot.org/") {
                "paste.dabbot.org"
            } else {
                &self.variables.dump_display_address
            };

            (host, uuid)
        };

        let body = match await!(self.dump.retrieve(uuid, &host)) {
            Ok(body) => body,
            Err(why) => {
                warn!("Err getting dump for {}: {:?}", uuid, why);

                return await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error getting the playlist.",
                ));
            },
        };

        let tracks = serde_json::from_slice::<Vec<String>>(&body)?;
        let track_count = tracks.len();

        await!(self.queue.add_tracks(guild_id.to_string(), tracks))?;

        let mut content = format!("Loaded {} songs from the playlist!", track_count);

        let req = JoinRequest {
            audio: &self.audio,
            bot_id: self.variables.discord_user_id,
            cache: &self.cache,
            channel_id: cmd.channel_id(),
            pop: true,
            queue: &self.queue,
            user_id: cmd.base.author.id.0,
            guild_id,
        };

        match await!(join_shared::join(&req)) {
            Ok(resp) => {
                write!(content, "\n\n{}", resp)?;
            },
            Err(why) => {
                warn!("Err joining voice channel: {:?}", why);
            },
        }

        await!(self.discord.send(cmd.channel_id(), content))
    }
}

command!(Load);

fn main() {
    Load::default().run();
}
