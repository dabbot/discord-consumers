#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;

#[derive(Default)]
pub struct Restart {
    audio: AudioClient,
    discord: DiscordApiClient,
}

impl Restart {
    pub const NAME: &'static str = "restart";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        match await!(self.audio.seek(cmd.guild_id, 0)) {
            Ok(()) => {
                await!(self.discord.send(
                    cmd.channel_id(),
                    "Restarted the song!",
                ))
            },
            Err(why) => {
                warn!("Err restarting song for {}: {:?}", cmd.guild_id, why);

                await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error restarting the song.",
                ))
            },
        }
    }
}

command!(Restart);

fn main() {
    Restart::default().run();
}
