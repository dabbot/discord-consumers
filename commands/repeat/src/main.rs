#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;
use cache::model::LoopMode;

#[derive(Default)]
pub struct Repeat {
    cache: CacheClient,
    discord: DiscordApiClient,
}

impl Repeat {
    pub const NAME: &'static str = "repeat";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let guild_id = cmd.guild_id;

        let mode = match await!(self.cache.get_loop_mode(cmd.guild_id)) {
            Ok(mode) => mode,
            Err(why) => {
                warn!("Err getting loop mode for {}: {:?}", guild_id, why);

                return await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error updating your loop setting!",
                ));
            },
        };

        let new_mode = match cmd.args.first().as_ref().map(|x| &**x) {
            Some(arg) => match &arg[..] {
                "one" | "song" => LoopMode::Song,
                "all" | "queue" => LoopMode::Queue,
                "off" | "stop" => LoopMode::Off,
                _ => {
                    return await!(self.discord.send(
                        cmd.channel_id(),
                        format!(
                            "That doesn't look like a valid command.\n\n{}",
                            Self::info(&cmd.prefix),
                        ),
                    ));
                },
            },
            None => match mode {
                Some(LoopMode::Queue) | Some(LoopMode::Song) => LoopMode::Off,
                Some(LoopMode::Off) | None => LoopMode::Queue,
            },
        };

        self.cache.set_loop_mode(guild_id, new_mode);

        let msg = match new_mode {
            LoopMode::Queue => "**Enabled** queue repeating.",
            LoopMode::Song => "**Enabled** song repeating.",
            LoopMode::Off => "**Disabled** repeating.",
        };

        await!(self.discord.send(
            cmd.channel_id(),
            format!("{}\n\n{}", msg, Self::info(&cmd.prefix)),
        ))
    }

    fn info(prefix: &str) -> String {
        format!("To toggle repeating, use `{prefix}repeat`. To repeat the queue, use `{prefix}loop queue`.
To repeat one song, use `{prefix}repeat song`.
To turn off song/queue repeating, use `{prefix}repeat off`.", prefix=prefix)
    }
}

command!(Repeat);

fn main() {
    Repeat::default().run();
}
