#![feature(async_await, await_macro, futures_api)]

use audio::Error as AudioError;
use base::prelude::*;
use cache::Error as CacheError;
use serde_json::{Error as JsonError, json};
use serenity::constants::VoiceOpCode;
use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
};

#[derive(Debug)]
pub enum LeaveError {
    Audio(AudioError),
    Cache(CacheError),
    Json(JsonError),
}

impl From<CacheError> for LeaveError {
    fn from(e: CacheError) -> Self {
        LeaveError::Cache(e)
    }
}

impl From<JsonError> for LeaveError {
    fn from(e: JsonError) -> Self {
        LeaveError::Json(e)
    }
}

impl From<AudioError> for LeaveError {
    fn from(e: AudioError) -> Self {
        LeaveError::Audio(e)
    }
}

impl Display for LeaveError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        use LeaveError::*;

        f.write_str(match self {
            Audio(e) => e.description(),
            Cache(e) => e.description(),
            Json(e) => e.description(),
        })
    }
}

impl StdError for LeaveError {}

pub async fn leave<'a>(
    shard_id: u64,
    guild_id: u64,
    audio: &'a AudioClient,
    cache: &'a CacheClient,
    bot_id: u64,
) -> Result<(), LeaveError> {
    let map = serde_json::to_vec(&json!({
        "op": VoiceOpCode::SessionDescription.num(),
        "d": {
            "channel_id": None::<Option<u64>>,
            "guild_id": guild_id,
            "self_deaf": true,
            "self_mute": false,
        },
    }))?;

    await!(cache.sharder_msg(shard_id, map))?;
    await!(cache.delete_join(guild_id))?;
    await!(cache.delete_choices(guild_id))?;
    await!(cache.delete_voice_state(guild_id, bot_id))?;

    await!(audio.stop(guild_id))?;

    Ok(())
}
