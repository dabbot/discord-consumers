#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;

#[derive(Default)]
pub struct Leave {
    audio: Arc<AudioClient>,
    cache: Arc<CacheClient>,
    discord: DiscordApiClient,
    variables: Variables,
}

impl Leave {
    pub const NAME: &'static str = "Leave";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let gid = cmd.guild_id;
        let sid = cmd.shard_id;
        let bid = self.variables.discord_user_id;

        match await!(leave_shared::leave(sid, gid, &self.audio, &self.cache, bid)) {
            Ok(()) => {
                await!(self.discord.send(
                    cmd.channel_id(),
                    "Stopped playing music & left the voice channel.",
                ))
            },
            Err(why) => {
                error!("Error stopping in guild {}: {:?}", gid, why,);

                await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error leaving the voice channel.",
                ))
            },
        }
    }
}

command!(Leave);

fn main() {
    Leave::default().run();
}
