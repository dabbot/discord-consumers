#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;
use std::fmt::Display;

#[derive(Default)]
pub struct Reorder {
    discord: DiscordApiClient,
    queue: QueueClient,
}

impl Reorder {
    pub const NAME: &'static str = "reorder";

    pub async fn process(self: Arc<Self>, mut cmd: Command) -> BaseResult {
        let cid = cmd.channel_id();

        if cmd.args.len() != 2 {
            return await!(self.discord.send(
                cid,
                "You need to give two song positions to reorder!
Refer to <https://dabbot.org/commands> for help.",
            ));
        }

        // Avoid a vector left-shift.
        let (two, one) = (cmd.args.remove(1), cmd.args.remove(0));

        let first = match one.parse::<u64>() {
            Ok(first) => first,
            Err(_) => return await!(self.non_number(cid, &one)),
        };
        let second = match two.parse::<u64>() {
            Ok(second) => second,
            Err(_) => return await!(self.non_number(cid, &two)),
        };

        match await!(self.queue.reorder(cmd.guild_id.to_string(), first, second)) {
            Ok(()) => {
                await!(self.discord.send(
                    cid,
                    "Reordered those songs!",
                ))
            },
            Err(why) => {
                warn!(
                    "Err reordering songs {}, {} in {}: {:?}",
                    first,
                    second,
                    cmd.guild_id,
                    why,
                );

                await!(self.discord.send(
                    cid,
                    "There was an error reordering those songs",
                ))
            },
        }
    }

    async fn non_number(
        self: Arc<Self>,
        channel_id: u64,
        arg: impl Display,
    ) -> BaseResult {
        await!(self.discord.send(
            channel_id,
            format!("{} isn't a valid number!", arg),
        ))
    }
}

command!(Reorder);

fn main() {
    Reorder::default().run();
}
