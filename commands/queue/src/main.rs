#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;
use core::fmt::Write;
use queue::model::{GetRequest, Song};
use serenity::utils::MessageBuilder;

#[derive(Default)]
pub struct Queue {
    audio: AudioClient,
    discord: DiscordApiClient,
    queue: QueueClient,
}

impl Queue {
    pub const NAME: &'static str = "queue";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        trace!("Requested page: {:?}", cmd.args.first());

        let page = Self::calculate_page(cmd.args.first().map(|x| &**x));
        let start = page * 10;

        let req = GetRequest {
            guild_id: cmd.guild_id,
            limit: Some(10),
            offset: Some(start),
        };

        let queue = match await!(self.queue.get(req)) {
            Ok(queue) => queue,
            Err(why) => {
                warn!("Err getting queue for {}: {:?}", cmd.guild_id, why);

                return await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error getting the queue.",
                ));
            },
        };

        let mut msg = MessageBuilder::new();

        match await!(self.audio.current(cmd.guild_id)) {
            Ok(current) => {
                write!(msg.0, "{}", current)?;
            },
            Err(why) => {
                warn!(
                    "Err getting current music for {}: {:?}",
                    cmd.guild_id,
                    why,
                );

                msg.0.push_str("There was an error getting the current song.");
            },
        }

        msg.0.push_str("\n\n__Queue__:\n");

        if page == 0 && queue.is_empty() {
            msg.0.push_str("There are no songs in the queue.");
        } else if queue.is_empty() {
            msg.0.push_str("There are no songs on this page of the queue.");
        } else {
            Self::format_queue(queue, &mut msg, start as usize)?;
        }

        if msg.0.len() > 2000 {
            msg.0.truncate(1997);
            msg.0.push_str("...");
        }

        await!(self.discord.send(cmd.channel_id(), msg.build()))
    }

    fn calculate_page(arg: Option<&str>) -> u64 {
        let mut requested = arg.and_then(|x| x.parse().ok()).unwrap_or(0);

        if requested > 0 {
            requested -= 1;
        }

        requested
    }

    fn format_queue(
        queue: impl IntoIterator<Item = Song>,
        msg: &mut MessageBuilder,
        start: usize,
    ) -> BaseResult {
        for (idx, item) in queue.into_iter().enumerate() {
            write!(msg.0, "`{:02}`", start + idx + 1)?;

            msg.0.push(' ');
            msg.push_bold_safe(item.info.title);
            msg.0.push_str(" by ");
            msg.push_bold_safe(item.info.author);
            msg.0.push(' ');

            let length = audio_utils::track_length_readable(
                item.info.length as u64,
            );
            msg.push_mono_safe(format!("[{}]", length));
            msg.0.push('\n');
        }

        Ok(())
    }
}

command!(Queue);

fn main() {
    Queue::default().run();
}

#[cfg(test)]
mod tests {
    use base::prelude::*;
    use lavalink::decoder::DecodedTrack;
    use super::{Queue, Song};

    #[test]
    fn test_page_0() {
        assert_eq!(Queue::calculate_page(Some("0")), 0);
        assert_eq!(Queue::calculate_page(Some("1")), 0);
    }

    #[test]
    fn test_page_no_arg() {
        assert_eq!(Queue::calculate_page(None), 0);
    }

    #[test]
    fn test_page_numbered() {
        assert_eq!(Queue::calculate_page(Some("7")), 6);
        assert_eq!(Queue::calculate_page(Some("1500")), 1499);
    }

    #[test]
    fn test_queue_one_song() -> BaseResult {
        let item = vec![Song {
            info: DecodedTrack {
                author: "xKito Music".to_owned(),
                identifier: "zcn4-taGvlg".to_owned(),
                length: 184_000,
                source: "youtube".to_owned(),
                stream: false,
                title: "she - Prismatic".to_owned(),
                url: "https://www.youtube.com/watch?v=zcn4-taGvlg".to_owned().into(),
                version: 1,
            },
            track_base64: "QAAAdAIAD3NoZSAtIFByaXNtYXRpYwALeEtpdG8gTXVzaWMAAAAAAA\
LOwAALemNuNC10YUd2bGcAAQAraHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj16Y240LXRhR\
3ZsZwAHeW91dHViZQAAAAAAAAAA".to_owned(),
        }];

        let mut msg = MessageBuilder::new();

        Queue::format_queue(item, &mut msg, 0)?;

        assert_eq!(
            msg.build(),
            "`01` **she - Prismatic** by **xKito Music** `[3m 4s]`\n",
        );

        Ok(())
    }
}
