#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;

#[derive(Default)]
pub struct Resume {
    audio: AudioClient,
    discord: DiscordApiClient,
}

impl Resume {
    pub const NAME: &'static str = "resume";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let resp = match await!(self.audio.resume(cmd.guild_id)) {
            Ok(()) => "Resumed music playback!",
            Err(why) => {
                warn!("Error resuming guild {}: {:?}", cmd.guild_id, why);

                "There was an error resuming the music."
            },
        };

        await!(self.discord.send(cmd.channel_id(), resp))
    }
}

command!(Resume);

fn main() {
    Resume::default().run();
}
