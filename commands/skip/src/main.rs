#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;

#[derive(Default)]
pub struct Skip {
    audio: AudioClient,
    discord: DiscordApiClient,
}

impl Skip {
    pub const NAME: &'static str = "skip";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        match await!(self.audio.skip(cmd.guild_id)) {
            Ok(()) => await!(self.discord.send(cmd.channel_id(), "Skipped")),
            Err(why) => {
                warn!("Error skipping guild {}: {:?}", cmd.guild_id, why);

                await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error skipping the song.",
                ))
            },
        }
    }
}

command!(Skip);

fn main() {
    Skip::default().run();
}
