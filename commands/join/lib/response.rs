use crate::{
    error::Error,
    JoinState,
    JoinRequest,
    PopStatus,
};
use std::fmt::{Display, Formatter, Result as FmtResult};

pub struct JoinResponse<'a> {
    pub pop: PopStatus,
    pub request: &'a JoinRequest<'a>,
    pub state: JoinState,
}

impl<'a> JoinResponse<'a> {
    pub(crate) fn already_in_channel(request: &'a JoinRequest<'a>) -> Self {
        Self {
            pop: PopStatus::None,
            state: JoinState::AlreadyInChannel,
            request,
        }
    }

    pub(crate) fn already_playing(request: &'a JoinRequest<'a>) -> Self {
        Self {
            pop: PopStatus::AlreadyPlaying,
            state: JoinState::Successful,
            request,
        }
    }

    pub(crate) fn empty_pop(request: &'a JoinRequest<'a>) -> Self {
        Self {
            pop: PopStatus::None,
            state: JoinState::Successful,
            request,
        }
    }

    pub(crate) fn error_playing(request: &'a JoinRequest<'a>, why: Error) -> Self {
        Self {
            pop: PopStatus::ErrorPlaying(why),
            state: JoinState::Successful,
            request,
        }
    }

    pub(crate) fn error_popping(request: &'a JoinRequest<'a>, why: Error) -> Self {
        Self {
            pop: PopStatus::ErrorPopping(why),
            state: JoinState::Successful,
            request,
        }
    }

    pub(crate) fn not_in_channel(request: &'a JoinRequest<'a>) -> Self {
        Self {
            pop: PopStatus::None,
            state: JoinState::UserNotInChannel,
            request
        }
    }

    pub(crate) fn playing_next(request: &'a JoinRequest<'a>) -> Self {
        Self {
            pop: PopStatus::Playing,
            state: JoinState::Successful,
            request,
        }
    }

    pub(crate) fn queued(request: &'a JoinRequest<'a>) -> Self {
        Self {
            pop: PopStatus::Queued,
            state: JoinState::Successful,
            request,
        }
    }

    pub(crate) fn successful_without_pop(request: &'a JoinRequest<'a>) -> Self {
        Self {
            pop: PopStatus::None,
            state: JoinState::Successful,
            request,
        }
    }
}

impl<'a> Display for JoinResponse<'a> {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self.state {
            JoinState::AlreadyInChannel => {
                return f.write_str("Joined the voice channel.");
            },
            JoinState::UserNotInChannel => {
                return f.write_str("It looks like you aren't in a voice channel.");
            },
            JoinState::Successful => {},
        }

        f.write_str(match self.pop {
            PopStatus::AlreadyPlaying => "A song is already playing.",
            PopStatus::ErrorPopping(_) => {
                "There was an error getting the next song in the queue."
            },
            PopStatus::ErrorPlaying(_) => {
                "There was an error playing the song."
            },
            PopStatus::None => "There was nothing in the queue to play.",
            PopStatus::Playing => "Joined the voice channel and now playing the next song in the queue!",
            PopStatus::Queued => "Added the song to the queue!",
        })
    }
}
