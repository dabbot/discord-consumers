use audio::Error as AudioError;
use base::prelude::*;
use cache::Error as CacheError;
use queue::Error as QueueError;
use serde_json::Error as JsonError;
use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    result::Result as StdResult,
};

pub type Result<T> = StdResult<T, Error>;

#[derive(Debug)]
pub enum Error {
    Audio(AudioError),
    Cache(CacheError),
    Json(JsonError),
    Queue(QueueError),
}

impl From<CacheError> for Error {
    fn from(e: CacheError) -> Self {
        Error::Cache(e)
    }
}

impl From<JsonError> for Error {
    fn from(e: JsonError) -> Self {
        Error::Json(e)
    }
}

impl From<AudioError> for Error {
    fn from(e: AudioError) -> Self {
        Error::Audio(e)
    }
}

impl From<QueueError> for Error {
    fn from(e: QueueError) -> Self {
        Error::Queue(e)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        use Error::*;

        f.write_str(match self {
            Audio(e) => e.description(),
            Cache(e) => e.description(),
            Json(e) => e.description(),
            Queue(e) => e.description(),
        })
    }
}

impl StdError for Error {}
