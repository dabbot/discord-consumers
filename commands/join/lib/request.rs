use base::prelude::*;

pub struct JoinRequest<'a> {
    pub audio: &'a AudioClient,
    pub bot_id: u64,
    pub cache: &'a CacheClient,
    pub channel_id: u64,
    pub guild_id: u64,
    pub pop: bool,
    pub queue: &'a QueueClient,
    pub user_id: u64,
}
