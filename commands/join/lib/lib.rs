#![feature(async_await, await_macro, futures_api)]

mod error;
mod request;
mod response;

pub use error::{Error as JoinError, Result as JoinResult};
pub use request::JoinRequest;
pub use response::JoinResponse;

use base::prelude::*;
use error::{Error, Result};
use serde_json::json;
use serenity::constants::VoiceOpCode;

pub enum PopStatus {
    AlreadyPlaying,
    ErrorPopping(Error),
    ErrorPlaying(Error),
    None,
    Playing,
    Queued,
}

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq, PartialOrd, Ord)]
pub enum Join {
    AlreadyInChannel,
    Successful,
    UserNotInChannel,
}

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq, PartialOrd, Ord)]
pub enum JoinState {
    AlreadyInChannel,
    Successful,
    UserNotInChannel,
}

pub async fn join<'a>(req: &'a JoinRequest<'a>) -> Result<JoinResponse<'a>> {
    let JoinRequest {
        audio,
        bot_id,
        cache,
        channel_id,
        guild_id,
        pop,
        queue,
        user_id,
    } = *req;

    trace!("Checking if G:{};U:{} is in a voice channel", guild_id, user_id);

    // Check if the user is in a voice channel.
    let user = match await!(cache.get_voice_state(guild_id, user_id))? {
        Some(user) => user,
        None => return Ok(JoinResponse::not_in_channel(req)),
    };

    trace!("User voice state: {:?}", user);

    trace!("Checking if bot is already in voice channel");
    // Check if the bot is already in the requested channel.
    if let Some(bot) = await!(cache.get_voice_state(guild_id, bot_id))? {
        trace!(
            "Bot's channel ID: {:?}; user's channel ID: {:?}",
            bot.channel_id,
            user.channel_id,
        );

        if bot.channel_id == user.channel_id {
            trace!("Bot is in user voice channel already");
            return Ok(JoinResponse::already_in_channel(req));
        }
    } else {
        trace!("Bot not in user's voice channel");
    }

    trace!("Serializing audio player for guild {}", guild_id);
    let map = serde_json::to_vec(&json!({
        "op": VoiceOpCode::SessionDescription.num(),
        "d": {
            "channel_id": user.channel_id,
            "guild_id": guild_id,
            "self_deaf": true,
            "self_mute": false,
        },
    }))?;
    trace!("Serialized audio player");
    trace!("Sending SessionDescription payload to sharder: {:?}", map);
    // TODO
    // await!(req.to_sharder(map))?;
    trace!("Sent SessionDescription payload to sharder");

    trace!("Setting join in cache");
    await!(cache.set_join(guild_id, channel_id))?;
    trace!("Set join in cache");

    if !pop {
        trace!("Join successful without pop");

        return Ok(JoinResponse::successful_without_pop(req));
    }

    trace!("Joining with pop");

    trace!("Getting current voice state");
    let current = await!(audio.current(guild_id))?;
    trace!("Got current voice state");

    if current.is_playing() {
        trace!("Already playing");

        return Ok(JoinResponse::already_playing(req));
    }

    trace!("Not yet playing a song");

    let song = match await!(queue.pop(guild_id.to_string())) {
        Ok(Some(song)) => song,
        Ok(None) => return Ok(JoinResponse::empty_pop(req)),
        Err(why) => {
            let err = From::from(why);

            return Ok(JoinResponse::error_popping(req, err));
        },
    };

    match await!(audio.play(guild_id, song.track_base64)) {
        Ok(true) => Ok(JoinResponse::playing_next(req)),
        Ok(false) => Ok(JoinResponse::queued(req)),
        Err(why) => Ok(JoinResponse::error_playing(req, From::from(why))),
    }
}
