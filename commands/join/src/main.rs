#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;
use join_shared::JoinRequest;

#[derive(Default)]
pub struct Join {
    audio: AudioClient,
    cache: CacheClient,
    discord: DiscordApiClient,
    queue: QueueClient,
    variables: Variables,
}

impl Join {
    pub const NAME: &'static str = "join";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let request = JoinRequest {
            audio: &self.audio,
            bot_id: self.variables.discord_user_id,
            cache: &self.cache,
            channel_id: cmd.channel_id(),
            guild_id: cmd.guild_id,
            pop: true,
            queue: &self.queue,
            user_id: cmd.base.author.id.0,
        };

        match await!(join_shared::join(&request)) {
            Ok(response) => {
                await!(self.discord.send(
                    cmd.channel_id(),
                    response.to_string(),
                ))
            },
            Err(why) => {
                warn!("Err joining channel: {:?}", why);

                await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error joining the channel!",
                ))
            },
        }
    }
}

command!(Join);

fn main() {
    Join::default().run();
}
