#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;

#[derive(Default)]
pub struct Volume {
    audio: AudioClient,
    discord: DiscordApiClient,
    patron: PatronApiClient,
}

impl Volume {
    pub const NAME: &'static str = "volume";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        if !await!(self.patron.patron(cmd.base.author.id.0))?.active {
            return await!(self.discord.send(
                cmd.channel_id(),
                r#"**The volume command is dabBot premium only!**
    Donate for the `Volume Control` tier on Patreon at https://patreon.com/dabbot to gain access."#,
            ));
        }

        match cmd.args.len() {
            0 => await!(self.current(cmd)),
            1 => await!(self.update(cmd)),
            _ => {
                return await!(self.discord.send(
                    cmd.channel_id(),
                    "You supplied too many arguments!",
                ));
            },
        }
    }

    async fn current(self: Arc<Self>, cmd: Command) -> BaseResult {
        let resp = match await!(self.audio.current(cmd.guild_id)) {
            Ok(player) => {
                format!("The volume is currently {}", player.volume)
            },
            Err(why) => {
                warn!("Err getting current player: {:?}", why);

                "There was an error getting the volume".into()
            },
        };

        await!(self.discord.send(cmd.channel_id(), resp))
    }

    async fn update(self: Arc<Self>, cmd: Command) -> BaseResult {
        let volume = match cmd.args[0].parse::<u64>() {
            Ok(volume @ 0...150) => volume,
            Ok(_) | Err(_) => {
                return await!(self.discord.send(
                    cmd.channel_id(),
                    "The volume must be between 0 and 150",
                ));
            },
        };

        match await!(self.audio.volume(cmd.guild_id, volume)) {
            Ok(()) => {
                await!(self.discord.send(
                    cmd.channel_id(),
                    "Updated the volume.",
                ))
            },
            Err(why) => {
                warn!(
                    "Error updating volume to {} for {}: {:?}",
                    volume,
                    cmd.guild_id,
                    why,
                );

                await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error updating the volume.",
                ))
            },
        }
    }
}

command!(Volume);

fn main() {
    Volume::default().run();
}
