// - `async_await` is needed for `async fn` syntax.
// - `await_macro` for the `await!()` macro
// - `futures_api` to use futures 0.3
#![feature(async_await, await_macro, futures_api)]

// Import some useful, commonly-used types. Useful for quick "prototyping."
use base::prelude::*;

// Struct container of API clients.
//
// This must not contain any state! Assume that the container may die at any
// time.
#[derive(Default)]
pub struct Ping {
    discord: DiscordApiClient,
}

impl Ping {
    // The name of the command. This is so the kafka consumer knows what topic
    // to consume.
    pub const NAME: &'static str = "ping";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        await!(self.discord.send(cmd.channel_id(), "Pong!"))
    }
}

command!(Ping);

fn main() {
    Ping::default().run();
}
