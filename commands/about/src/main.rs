#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;

#[derive(Default)]
pub struct About {
    discord: DiscordApiClient,
}

impl About {
    pub const NAME: &'static str = "about";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let msg = format!(
            "**dabBot**
Command prefix: `{}`
Invite me to your server: <https://dabbot.org/invite>
Support server: <https://dabbot.org/support>
Github: <https://github.com/dabbotorg>",
            cmd.prefix,
        );

        await!(self.discord.send(cmd.channel_id(), msg))
    }
}

command!(About);

fn main() {
    About::default().run();
}
