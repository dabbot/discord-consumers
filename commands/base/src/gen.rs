use std::fmt::Display;

pub fn command_topic(name: impl Display) -> String {
    format!("dabbotorg.dabbot.command.{}", name)
}
