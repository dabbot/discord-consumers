use kafka::error::KafkaError;
use serde_json::Error as JsonError;
use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    result::Result as StdResult,
};

pub type Result<T> = StdResult<T, Error>;

#[derive(Debug)]
pub enum Error {
    Kafka(KafkaError),
    KafkaStreamEmpty,
    Json(JsonError),
    NoPayload,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        f.write_str(self.description())
    }
}

impl StdError for Error {
    fn description(&self) -> &str {
        match self {
            Error::Json(e) => e.description(),
            Error::Kafka(e) => e.description(),
            Error::KafkaStreamEmpty => "Stream produced empty message",
            Error::NoPayload => "Kafka message contains no payload",
        }
    }
}

impl From<JsonError> for Error {
    fn from(e: JsonError) -> Self {
        Error::Json(e)
    }
}

impl From<KafkaError> for Error {
    fn from(e: KafkaError) -> Self {
        Error::Kafka(e)
    }
}
