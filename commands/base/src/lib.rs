#![feature(async_await, await_macro, decl_macro, futures_api)]

pub extern crate futures;
pub extern crate kafka;
pub extern crate serenity;
pub extern crate tokio;

pub mod model;
pub mod prelude;

mod error;
mod gen;

pub use self::error::{Error, Result};
pub use model::Command;

use core::{
    pin::Pin,
    task::LocalWaker,
};
use futures::{Poll, Stream};
use futures01::{Async as Async01, Stream as _};
use kafka::{
    config::ClientConfig,
    consumer::{
        Consumer as KafkaConsumer,
        DefaultConsumerContext,
        MessageStream,
        StreamConsumer,
    },
    Message,
};
use std::{
    error::Error as StdError,
    result::Result as StdResult,
};

pub struct Watcher<'a>(MessageStream<'a, DefaultConsumerContext>);

impl<'a> Stream for Watcher<'a> {
    type Item = Result<Command>;

    fn poll_next(mut self: Pin<&mut Self>, _: &LocalWaker) -> Poll<Option<Self::Item>> {
        let msg = {
            let mut this = Pin::as_mut(&mut self);

            match this.0.poll() {
                Ok(Async01::Ready(Some(msg))) => msg?,
                Ok(Async01::Ready(None)) => return Poll::Ready(None),
                Ok(Async01::NotReady) => return Poll::Pending,
                Err(why) => unreachable!("Err with stream: {:?}", why),
            }
        };

        let payload = msg.payload().ok_or(Error::NoPayload)?;
        let command = serde_json::from_slice(payload)?;

        Poll::Ready(Some(Ok(command)))
    }
}

pub struct Consumer(StreamConsumer<DefaultConsumerContext>);

impl Consumer {
    pub fn new(
        brokers: impl AsRef<str>,
        group_id: impl AsRef<str>,
        name: impl AsRef<str>,
    ) -> Result<Consumer> {
        let consumer: StreamConsumer = ClientConfig::new()
            .set("group.id", group_id.as_ref())
            .set("bootstrap.servers", brokers.as_ref())
            .set("enable.partition.eof", "false")
            .set("session.timeout.ms", "6000")
            .set("enable.auto.commit", "true")
            .create_with_context(DefaultConsumerContext)?;

        consumer.subscribe(&[&gen::command_topic(name.as_ref())])?;

        Ok(Self(consumer))
    }

    pub fn watcher<'a>(&'a self) -> Watcher<'a> {
        Watcher(self.0.start())
    }
}

pub type BaseResult = StdResult<(), Box<dyn StdError>>;

#[macro_export]
macro_rules! command {
    ($struct:ident) => {
        impl $struct {
            fn run(self) {
                tokio::run_async(async {
                    use $crate::futures::{FutureExt, TryFutureExt};
                    use ::std::sync::Arc;

                    let consumer = Consumer::new(
                        "0.0.0.0:9092",
                        "1",
                        $struct::NAME,
                    ).expect("Err making consumer");
                    let mut stream = consumer.watcher();
                    let this = Arc::new(self);

                    loop {
                        while let Some(Ok(cmd)) = await!(stream.next()) {
                            let this = Arc::clone(&this);

                            let ft = $struct::process(this, cmd).map_err(|why| {
                                warn!("Err with process: {:?}", why);
                            });

                            $crate::tokio::spawn(ft.boxed().compat());
                        }
                    }
                });
            }
        }
    };
}
