use serde::{Deserialize, Serialize};
use serenity::model::channel::Message;

/// A command received.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Command {
    pub args: Vec<String>,
    pub base: Message,
    pub guild_id: u64,
    pub name: String,
    pub prefix: String,
    pub shard_id: u64,
}

impl Command {
    pub fn channel_id(&self) -> u64 {
        self.base.channel_id.0
    }

    pub fn user_id(&self) -> u64 {
        self.base.author.id.0
    }
}
