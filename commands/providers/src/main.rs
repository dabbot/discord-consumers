#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;

#[derive(Default)]
pub struct Providers {
    discord: DiscordApiClient,
}

impl Providers {
    pub const NAME: &'static str = "providers";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        await!(self.discord.send(
            cmd.channel_id(),
            "Available music providers: youtube, soundcloud, bandcamp, vimeo, \
            twitch, beam.pro, http",
        ))
    }
}

command!(Providers);

fn main() {
    Providers::default().run();
}
