use std::fmt::{Display, Formatter, Result as FmtResult};

#[derive(Clone, Debug)]
pub struct Add {
    pub author: String,
    pub length: u64,
    pub status: AddStatus,
    pub title: String,
}

impl Display for Add {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self.status {
            AddStatus::Enqueued => {
                f.write_fmt(format_args!(
                    "Added **{}** by **{}** `[{}]` to the queue.",
                    self.title,
                    self.author,
                    audio_utils::track_length_readable(self.length as u64),
                ))
            },
            AddStatus::Playing => {
                f.write_fmt(format_args!(
                    "Now playing **{}** by **{}** `[{}]`",
                    self.title,
                    self.author,
                    audio_utils::track_length_readable(self.length as u64),
                ))
            },
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum AddStatus {
    Enqueued,
    Playing,
}
