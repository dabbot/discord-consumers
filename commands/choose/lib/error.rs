use audio::Error as AudioError;
use base::prelude::*;
use cache::Error as CacheError;
use join_shared::JoinError;
use lavalink::Error as LavalinkError;
use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    result::Result as StdResult,
};

pub type Result<T> = StdResult<T, Error>;

#[derive(Debug)]
pub enum Error {
    Audio(AudioError),
    Cache(CacheError),
    Join(JoinError),
    Lavalink(LavalinkError),
}

impl From<AudioError> for Error {
    fn from(e: AudioError) -> Self {
        Error::Audio(e)
    }
}

impl From<CacheError> for Error {
    fn from(e: CacheError) -> Self {
        Error::Cache(e)
    }
}

impl From<JoinError> for Error {
    fn from(e: JoinError) -> Self {
        Error::Join(e)
    }
}

impl From<LavalinkError> for Error {
    fn from(e: LavalinkError) -> Self {
        Error::Lavalink(e)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        use Error::*;

        f.write_str(match self {
            Audio(e) => e.description(),
            Cache(e) => e.description(),
            Join(e) => e.description(),
            Lavalink(e) => e.description(),
        })
    }
}

impl StdError for Error {}
