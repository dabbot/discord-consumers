#![feature(async_await, await_macro, futures_api)]

mod add;
mod error;

pub use add::{Add, AddStatus};
pub use error::{Error, Result};

use base::prelude::*;
use join_shared::JoinRequest;
use lavalink::decoder;

pub async fn select(track: String, req: JoinRequest) -> Result<Add> {
    await!(join_shared::join(&req))?;
    await!(cancel_shared::delete_selection(&req.cache, req.guild_id))?;

    await!(add(&req.audio, req.guild_id, track))
}

pub async fn add(audio: &AudioClient, guild_id: u64, track: String) -> Result<Add> {
    let song = decoder::decode_track_base64(&track)?;

    let status = if await!(audio.play(guild_id, track))? {
        AddStatus::Playing
    } else {
        AddStatus::Enqueued
    };

    Ok(Add {
        author: song.author,
        length: song.length,
        title: song.title,
        status,
    })
}
