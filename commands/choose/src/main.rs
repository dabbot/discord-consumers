#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;
use join_shared::JoinRequest;

#[derive(Default)]
pub struct Choose {
    audio: AudioClient,
    cache: CacheClient,
    discord: DiscordApiClient,
    queue: QueueClient,
    variables: Variables,
}

impl Choose {
    pub const NAME: &'static str = "choose";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let guild_id = cmd.guild_id;

        let mut selection: Vec<String> = match await!(self.cache.get_choices_ranged(guild_id, 0, 5)) {
            Ok(selection) => selection,
            Err(why) => {
                warn!("Err getting selection: {:?}", why);

                return await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error getting the choices.",
                ));
            },
        };

        if selection.is_empty() {
            let prefix = &cmd.prefix;

            let mut msg = MessageBuilder::new();
            // push_safe is used to filter @everyone and other pings
            msg.push_safe(format!("There's no selection active in this guild - are you sure you ran `{prefix}play`?
To play a song...
* Join a voice channel
* Use `{prefix}play <song name/link>`
* Choose one of the song options with `{prefix}choose <option>`", prefix=prefix));

            return await!(self.discord.send(cmd.channel_id(), msg.build()));
        }

        let mut numbers = Vec::with_capacity(5);

        for arg in cmd.args.iter() {
            let num = match arg.parse::<usize>() {
                Ok(num @ 1 ... 5) => num - 1,
                _ => {
                    return await!(self.discord.send(
                        cmd.channel_id(),
                        "You must provide one or more numbers between 1 and 5!",
                    ));
                },
            };

            numbers.push(num);

            if numbers.len() == numbers.capacity() {
                break;
            }
        }

        numbers.sort();
        numbers.dedup();
        numbers.reverse();

        if numbers.is_empty() {
            return await!(cancel_shared::delete_selection(
                &self.cache,
                guild_id,
            )).map_err(From::from);
        }

        let selections = numbers
            .into_iter()
            .map(|num| selection.remove(num))
            .collect::<Vec<_>>();
        let selection_count = selections.len();

        for track in selections.into_iter().rev() {
            await!(choose_shared::add(&self.audio, guild_id, track))?;
        }

        let req = JoinRequest {
            audio: &self.audio,
            bot_id: self.variables.discord_user_id,
            cache: &self.cache,
            channel_id: cmd.channel_id(),
            pop: false,
            queue: &self.queue,
            user_id: cmd.base.author.id.0,
            guild_id,
        };
        await!(join_shared::join(&req))?;
        await!(cancel_shared::delete_selection(&self.cache, guild_id))?;


        let plural = if selection_count == 1 {
            ""
        } else {
            "s"
        };

        await!(self.discord.send(cmd.channel_id(),
            format!(
                "Added {} song{} from the selection!",
                selection_count,
                plural,
            ),
        ))
    }
}

command!(Choose);

fn main() {
    Choose::default().run();
}
