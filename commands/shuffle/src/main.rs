#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;
use queue::model::GetRequest;

#[derive(Default)]
pub struct Shuffle {
    discord: DiscordApiClient,
    queue: QueueClient,
}

impl Shuffle {
    pub const NAME: &'static str = "shuffle";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let req = GetRequest {
            guild_id: cmd.guild_id,
            limit: Some(2),
            offset: None,
        };

        match await!(self.queue.get(req)) {
            Ok(queue) => {
                if queue.len() < 2 {
                    return await!(self.discord.send(
                        cmd.channel_id(),
                        "To shuffle, you need at least 2 songs in the queue!",
                    ));
                }
            },
            Err(why) => {
                warn!("Error getting the queue of {}: {:?}", cmd.guild_id, why);

                return await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error getting your queue.",
                ));
            }
        }

        let resp = match await!(self.queue.shuffle(cmd.guild_id.to_string())) {
            Ok(()) => "Shuffled the song queue.",
            Err(why) => {
                warn!("Error shuffling guild {}: {:?}", cmd.guild_id, why);

                "There was an error shuffling the song."
            },
        };

        await!(self.discord.send(cmd.channel_id(), resp))
    }
}

command!(Shuffle);

fn main() {
    Shuffle::default().run();
}
