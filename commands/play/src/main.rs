#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;
use play_shared::{PlayRequest, Provider};

#[derive(Default)]
pub struct Play {
    audio: AudioClient,
    cache: CacheClient,
    discord: DiscordApiClient,
    queue: QueueClient,
    variables: Variables,
}

impl Play {
    pub const NAME: &'static str = "play";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let req = PlayRequest {
            audio: &self.audio,
            bot_id: self.variables.discord_user_id,
            cache: &self.cache,
            cmd: &cmd,
            discord: &self.discord,
            pop: true,
            provider: Provider::YouTube,
            queue: &self.queue,
        };

        await!(play_shared::base(&req))
    }
}

command!(Play);

fn main() {
    Play::default().run();
}
