use base::prelude::*;
use crate::Provider;

pub struct Request<'a> {
    pub audio: &'a AudioClient,
    pub bot_id: u64,
    pub cache: &'a CacheClient,
    pub cmd: &'a Command,
    pub discord: &'a DiscordApiClient,
    pub pop: bool,
    pub provider: Provider,
    pub queue: &'a QueueClient,
}
