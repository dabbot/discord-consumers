use std::fmt::{Display, Formatter, Result as FmtResult, Write as _};

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum Provider {
    SoundCloud,
    URL,
    YouTube,
}

impl Provider {
    pub fn prefix(self) -> &'static str {
        use self::Provider::*;

        match self {
            SoundCloud => "scsearch",
            URL => "",
            YouTube => "ytsearch"
        }
    }
}

impl Display for Provider {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        if *self == Provider::URL {
            return Ok(());
        }

        f.write_str(self.prefix())?;
        f.write_char(':')?;

        Ok(())
    }
}
