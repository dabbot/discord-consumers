#![feature(async_await, await_macro, futures_api)]

mod provider;
mod request;

pub use provider::Provider;
pub use request::Request as PlayRequest;

use audio::Error as AudioError;
use base::prelude::*;
use join_shared::{JoinRequest, JoinState};
use lavalink::rest::{Load, LoadType};
use std::fmt::Write;

pub async fn base<'a>(req: &'a PlayRequest<'a>) -> BaseResult {
    let PlayRequest {
        audio,
        bot_id: _,
        cache: _,
        cmd,
        discord,
        pop: _,
        mut provider,
        queue: _,
    } = *req;

    if cmd.args.len() < 1 {
        return await!(discord.send(
            cmd.channel_id(),
            "You need to say the link to the song or the name of what you want to play",
        ));
    }

    let query = cmd.args.join(" ");

    if query.starts_with("https://") || query.starts_with("http://") {
        provider = Provider::URL;
    }

    let load = match await!(search(&audio, &query, provider)) {
        Ok(load) => load,
        Err(why) => {
            warn!(
                "Err searching tracks for query '{}' in provider {}: {:?}",
                query,
                provider.to_string(),
                why,
            );

            return await!(discord.send(
                cmd.channel_id(),
                "There was an error searching for that.",
            ));
        },
    };

    debug!("load: {:?}", load.load_type);

    match load.load_type {
        LoadType::LoadFailed => {
            return await!(discord.send(
                cmd.channel_id(),
                "There was an error searching for that!",
            ));
        },
        LoadType::NoMatches => {
            return await!(discord.send(
                cmd.channel_id(),
                "It looks like there aren't any results for that!",
            ));
        },
        LoadType::PlaylistLoaded => await!(handle_playlist(req, load)),
        LoadType::SearchResult | LoadType::TrackLoaded => {
            await!(handle_search(req, load))
        },
    }
}

async fn handle_search<'a>(
    req: &'a PlayRequest<'a>,
    mut load: Load,
) -> BaseResult {
    let PlayRequest {
        audio,
        bot_id,
        cache,
        cmd,
        discord,
        pop: _,
        provider: _,
        queue,
    } = *req;

    let guild_id = cmd.guild_id;

    if load.tracks.is_empty() {
        return await!(discord.send(
            cmd.channel_id(),
            "It looks like there aren't any results for that!",
        ));
    }

    if load.tracks.len() == 1 {
        let add = await!(choose_shared::select(
            load.tracks.remove(0).track,
            JoinRequest {
                audio: &audio,
                cache: &cache,
                channel_id: cmd.channel_id(),
                pop: true,
                queue: &queue,
                user_id: cmd.user_id(),
        bot_id,
                guild_id,
            }
        ))?;

        return await!(discord.send(cmd.channel_id(), add.to_string()));
    }

    load.tracks.truncate(5);

    let blobs = load
        .tracks
        .iter()
        .map(|t| t.track.clone())
        .rev()
        .collect::<Vec<_>>();

    debug!("Deleting existing choose for guild {}", guild_id);
    await!(cache.delete_choices(guild_id))?;
    debug!("Deleted existing choose for guild {}", guild_id);
    debug!("Setting choose for guild {}", guild_id);
    await!(cache.push_choices(guild_id, blobs))?;
    debug!("Set choose for guild {}", guild_id);

    let mut msg = MessageBuilder::new();

    for (idx, track) in load.tracks.iter().enumerate() {
        write!(msg.0, "`{}` ", idx + 1)?;
        msg.push_safe(&track.info.title);
        msg.0.push_str(" by ");
        msg.push_safe(&track.info.author);
        write!(
            msg.0,
            " `[{}]`",
            audio_utils::track_length_readable(track.info.length as u64)
        )?;
        msg.0.push('\n');
    }

    msg.0.push_str("\n**To choose**, use `");
    msg.push_safe(&cmd.prefix);
    msg.0.push_str(
        "choose <number>`
Example: `",
    );
    msg.push_safe(&cmd.prefix);
    msg.0.push_str(
        "choose 2` would pick the second option.
**To cancel**, use `",
    );
    msg.push_safe(&cmd.prefix);
    msg.0.push_str("cancel`.");

    await!(discord.send(cmd.channel_id(), msg.build()))
}

async fn handle_playlist<'a>(
    req: &'a PlayRequest<'a>,
    load: Load,
) -> BaseResult {
    let PlayRequest {
        audio,
        bot_id,
        cache,
        cmd,
        discord,
        pop: _,
        provider: _,
        queue,
    } = *req;

    let guild_id = cmd.guild_id;

    if load.tracks.is_empty() {
        return await!(discord.send(
            cmd.channel_id(),
            "It looks like that playlist is empty!",
        ));
    }

    let tracks = load
        .tracks
        .iter()
        .map(|t| t.track.clone())
        .collect::<Vec<_>>();
    let track_count = tracks.len();

    await!(queue.add_tracks(guild_id.to_string(), tracks))?;

    let req = JoinRequest {
        audio: &audio,
        cache: &cache,
        channel_id: cmd.channel_id(),
        pop: false,
        queue: &queue,
        user_id: cmd.user_id(),
        bot_id,
        guild_id,
    };
    let join = await!(join_shared::join(&req))?;

    let mut content = format!("Loaded {} songs from the playlist", track_count);

    if let Some(name) = load.playlist_info.name {
        write!(content, " **{}**", name)?;
    }

    content.push('!');

    match join.state {
        JoinState::UserNotInChannel => {
            return await!(discord.send(cmd.channel_id(), content));
        },
        JoinState::AlreadyInChannel | JoinState::Successful => {},
    }

    let current = await!(audio.current(guild_id))?;

    if current.is_playing() {
        return await!(discord.send(cmd.channel_id(), content));
    }

    let song = match await!(queue.pop(guild_id.to_string())) {
        Ok(Some(song)) => song,
        Ok(None) | Err(_) => {
            return await!(discord.send(cmd.channel_id(), content));
        },
    };

    match await!(audio.play(guild_id, song.track_base64)) {
        Ok(true) => {
            content.push_str("\n\nJoined the voice channel and started playing the next song!");

            await!(discord.send(cmd.channel_id(), content))
        },
        Ok(false) => {
            content.push_str("\n\nJoined the voice channel and added the songs to the queue.");

            await!(discord.send(cmd.channel_id(), content))
        },
        Err(why) => {
            warn!("Err playing next song: {:?}", why);

            await!(discord.send(cmd.channel_id(), content))
        },
    }
}

pub async fn search<'a>(
    audio: &'a AudioClient,
    query: impl AsRef<str> + 'a,
    provider: Provider,
) -> Result<Load, AudioError> {
    let term = format!("{}{}", provider, query.as_ref());
    let encode = !term.contains('%');
    let load = await!(audio.search(term, encode))?;

    Ok(load)
}
