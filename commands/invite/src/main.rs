#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;

#[derive(Default)]
pub struct Invite {
    discord: DiscordApiClient,
}

impl Invite {
    pub const NAME: &'static str = "invite";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        await!(self.discord.send(
            cmd.channel_id(),
            "Invite dabBot: <https://dabbot.org/invite>",
        ))
    }
}

command!(Invite);

fn main() {
    Invite::default().run();
}
