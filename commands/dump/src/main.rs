#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;
use queue::model::GetRequest;

#[derive(Default)]
pub struct Dump {
    audio: AudioClient,
    discord: DiscordApiClient,
    dump: DumpClient,
    queue: QueueClient,
}

impl Dump {
    pub const NAME: &'static str = "dump";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let req = GetRequest {
            guild_id: cmd.guild_id,
            limit: None,
            offset: None,
        };

        let queue = match await!(self.queue.get(req)) {
            Ok(queue) => queue,
            Err(why) => {
                warn!(
                    "There was an error getting the queue for guild {}: {:?}",
                    cmd.guild_id,
                    why,
                );

                return await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error getting the queue.",
                ));
            },
        };

        let current = await!(self.audio.current(cmd.guild_id))?;

        let mut tracks = queue
            .into_iter()
            .map(|item| item.track_base64)
            .collect::<Vec<_>>();

        if let Some(track) = current.track_raw {
            tracks.insert(0, track);
        }

        trace!("Serializing dump tracks");
        let body = serde_json::to_vec_pretty(&tracks)?;
        trace!("Serialized dump tracks");

        let dump = await!(self.dump.dump(body))?;

        let msg = format!(
            "A dump of your song queue was created! Link: https://{addr}/{uuid}
Load this playlist with `{prefix}load https://{addr}/{uuid}`",
            addr = shared::env::dump_display_address(),
            prefix = cmd.prefix,
            uuid = dump.uuid,
        );

        await!(self.discord.send(cmd.channel_id(), msg))
    }
}

command!(Dump);

fn main() {
    Dump::default().run();
}
