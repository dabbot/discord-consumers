#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;

const HELP: &'static str = "A list of our commands is available here: \
<https://dabbot.org/commands>";

#[derive(Default)]
pub struct Help {
    discord: DiscordApiClient,
}

impl Help {
    pub const NAME: &'static str = "help";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        await!(self.discord.send(cmd.channel_id(), HELP))
    }
}

command!(Help);

fn main() {
    Help::default().run();
}
