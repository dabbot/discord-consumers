#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;
use cache::Error as CacheError;

pub async fn delete_selection(cache: &Cache, guild_id: u64) -> Result<(), CacheError> {
    await!(cache.delete_choices(guild_id))
}
