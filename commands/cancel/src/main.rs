#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;

#[derive(Default)]
pub struct Cancel {
    cache: CacheClient,
    discord: DiscordApiClient,
}

impl Cancel {
    pub const NAME: &'static str = "cancel";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        await!(cancel_shared::delete_selection(&self.cache, cmd.guild_id))?;

        await!(self.discord.send(cmd.channel_id(), "Selection cancelled!"))
    }
}

command!(Cancel);

fn main() {
    Cancel::default().run();
}
