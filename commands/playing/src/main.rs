#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;

#[derive(Default)]
pub struct Playing {
    audio: AudioClient,
    discord: DiscordApiClient,
}

impl Playing {
    pub const NAME: &'static str = "playing";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let state = match await!(self.audio.current(cmd.guild_id)) {
            Ok(state) => state,
            Err(why) => {
                warn!("Err getting state for {}: {:?}", cmd.guild_id, why);

                return await!(self.discord.send(
                    cmd.channel_id(),
                    "There was an error getting the current song.",
                ));
            },
        };

        debug!("Player state for {}: {:?}", cmd.guild_id, state);

        await!(self.discord.send(cmd.channel_id(), state.to_string()))
    }
}

command!(Playing);

fn main() {
    Playing::default().run();
}
