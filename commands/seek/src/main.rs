#![feature(async_await, await_macro, bind_by_move_pattern_guards, futures_api)]

use base::prelude::*;

const ERROR_SEEKING: &'static str = "There was an error seeking the song.";

#[derive(Default)]
pub struct Seek {
    audio: AudioClient,
    discord: DiscordApiClient,
}

impl Seek {
    pub const NAME: &'static str = "seek";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let guild_id = cmd.guild_id;

        match await!(self.audio.current(guild_id)) {
            Ok(player) if player.is_playing() => {},
            Ok(_) => {
                return await!(self.discord.send(
                    cmd.channel_id(),
                    "No music is queued or playing on this guild! Add some using `!!!play <song name/link>`",
                ));
            },
            Err(_) => {
                return await!(self.discord.send(
                    cmd.channel_id(),
                    ERROR_SEEKING,
                ));
            },
        };

        let arg = match cmd.args.first() {
            Some(arg) => arg,
            None => {
                return await!(self.discord.send(
                    cmd.channel_id(),
                    format!("You need to say where you want to seek to!
Example: `{}seek 3:40` to seek to 3 minutes and 40 seconds", cmd.prefix),
                ));
            },
        };

        let position = match Self::argument_time(arg) {
            Some(position) => position,
            None => {
                return await!(self.discord.send(
                    cmd.channel_id(),
                    format!("That doesn't look like a valid time format.
Example: `{}seek 3:40` to seek to 3 minutes and 40 seconds", cmd.prefix),
                ));
            },
        };

        let resp = match await!(self.audio.seek(guild_id, position)) {
            Ok(()) => {
                "Jumped to the specified position. Use `!!!playing` to see the current song & \
                position."
            },
            Err(why) => {
                warn!("Err seeking song for {} to {}: {:?}", guild_id, 0, why);

                ERROR_SEEKING
            },
        };

        await!(self.discord.send(cmd.channel_id(), resp))
    }

    fn argument_time(input: &str) -> Option<i64> {
        let parts = input.split(':').collect::<Vec<_>>();
        let part_count = parts.len();

        if part_count > 3 {
            return None;
        }

        let part_count = parts.len();

        let mut numbers = [0; 3];

        for (idx, part) in parts.into_iter().enumerate() {
            let num = part.parse::<u8>().ok()?;

            if num > 59 {
                return None;
            }

            numbers[idx] = num;
        }

        let mut hours = 0;
        let mut minutes = 0;
        let seconds;

        match part_count {
            1 => {
                seconds = numbers[0];
            },
            2 => {
                minutes = numbers[0];
                seconds = numbers[1];
            },
            3 => {
                hours = numbers[0];
                minutes = numbers[1];
                seconds = numbers[2];
            },
            _ => return None,
        }

        let mut time = i64::from(seconds);

        if minutes > 0 {
            time += i64::from(minutes) * 60;
        }

        if hours > 0 {
            time += i64::from(hours) * 60 * 60;
        }

        Some(time * 1000)
    }
}

command!(Seek);

fn main() {
    Seek::default().run();
}

#[cfg(test)]
mod tests {
    use super::Seek;

    #[test]
    fn test_argument_time() {
        assert_eq!(Seek::argument_time("1").unwrap(), 1000);
        assert_eq!(Seek::argument_time("2:3").unwrap(), 123_000);
        assert_eq!(Seek::argument_time("20:31").unwrap(), 1_231_000);
        assert_eq!(Seek::argument_time("2:34:21").unwrap(), 9_261_000);
        assert!(Seek::argument_time("hi").is_none());
        assert!(Seek::argument_time("1:30:20a").is_none());
    }
}
