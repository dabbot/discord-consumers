#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;
use queue::Error as QueueError;

#[derive(Default)]
pub struct Clear {
    discord: DiscordApiClient,
    queue: QueueClient,
}

impl Clear {
    pub const NAME: &'static str = "clear";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let gid = cmd.guild_id.to_string();

        let msg = match await!(self.queue.delete_queue(gid)) {
            Ok(()) | Err(QueueError::NotFound) => {
                "Cleared the song queue!"
            },
            Err(why) => {
                warn!("Err clearing queue for {}: {:?}", cmd.guild_id, why);

                "There was an error clearing the queue."
            },
        };

        await!(self.discord.send(cmd.channel_id(), msg))
    }
}

command!(Clear);

fn main() {
    Clear::default().run();
}
