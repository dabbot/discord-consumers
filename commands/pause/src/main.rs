#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;

#[derive(Default)]
pub struct Pause {
    audio: AudioClient,
    discord: DiscordApiClient,
}

impl Pause {
    pub const NAME: &'static str = "pause";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        let resp = match await!(self.audio.pause(cmd.guild_id)) {
            Ok(()) => "The music was paused. Use the `resume` command to play the music again.",
            Err(why) => {
                warn!("Error pausing guild id {}: {:?}", cmd.guild_id, why);

                "There was an error pausing the music."
            },
        };

        await!(self.discord.send(cmd.channel_id(), resp))
    }
}

command!(Pause);

fn main() {
    Pause::default().run();
}
