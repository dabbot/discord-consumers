#![feature(async_await, await_macro, futures_api)]

use base::prelude::*;
use join_shared::JoinRequest;
use rand::seq::SliceRandom;

#[derive(Default)]
pub struct DiscordFm {
    audio: AudioClient,
    cache: CacheClient,
    discord: DiscordApiClient,
    discord_fm: DiscordFmClient,
    queue: QueueClient,
    variables: Variables,
}

impl DiscordFm {
    pub const NAME: &'static str = "discordfm";

    pub async fn process(self: Arc<Self>, cmd: Command) -> BaseResult {
        if cmd.args.is_empty() {
            let msg = format!(
                "Uses a song playlist from the now defunct Discord.FM
Usage: `{}dfm <library>`
**Available libraries:**
{}", cmd.prefix, self.discord_fm.list);

            return await!(self.discord.send(cmd.channel_id(), msg));
        }

        let query = cmd.args.join(" ").to_lowercase();

        let library = match self.discord_fm.libraries.get(&query) {
            Some(library) => library,
            None => {
                return await!(self.discord.send(
                    cmd.channel_id(),
                    format!(
                        "Invalid library! Use `{}dfm` to see usage & libraries.",
                        cmd.prefix,
                    ),
                ));
            },
        };

        let amount = library.items.len();
        let mut tracks = library
            .items
            .iter()
            .map(|item| item.track.clone())
            .collect::<Vec<_>>();

        tracks.shuffle(&mut rand::thread_rng());

        debug!("Adding {} to queue for guild: {}", amount, cmd.guild_id);
        let songs = await!(self.queue.add_tracks(
            cmd.guild_id.to_string(),
            tracks,
        ))?;

        let req = JoinRequest {
            audio: &self.audio,
            bot_id: self.variables.discord_user_id,
            cache: &self.cache,
            channel_id: cmd.channel_id(),
            guild_id: cmd.guild_id,
            pop: true,
            queue: &self.queue,
            user_id: cmd.base.author.id.0,
        };
        if let Err(why) = await!(join_shared::join(&req)) {
            warn!("Err joining user channel: {:?}", why);
        }

        let song_count = songs.len();

        await!(self.discord.send(
            cmd.channel_id(),
            format!(
                "Added {} ({} songs) to the song queue.",
                library.name,
                song_count,
            ),
        ))
    }
}

command!(DiscordFm);

fn main() {
    DiscordFm::default().run();
}
